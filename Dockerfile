FROM python:slim
LABEL version="1.4"
LABEL description="Very Simple Password Generator. VSPG is an API providing relatively strong password using common words."
LABEL org.opencontainers.image.authors="Justin Briard"


WORKDIR /usr/src/vspg

COPY vspg/ . 

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8080

CMD [ "python", "./vspg-api.py" ]

