# Release Notes


# 1.0

First release

# 1.1
**New** 

 **- Dictionary :** you can choose from 3 dictionaries for your words
The new element *language* is availlable in the json body.
you can chose from :
- **en** : more than 370000 English words
- **fr** : more than 330000 French words (whith accents)
- **fr_simple** : more than 22000 French words (without accents)

# 1.2
**New** 
**- Endpoint :** New endpoint access_key
Generate *access_key* and *secret_key*

**Fixe** 
 **- Dictionary :** we remove the accents from the French dictionary (fr)
 **- number_of_numbers :** was limited to 9 numbers, now, sky is the limit

# 1.2.1
**New** 

**- friendly_separator :** New limitation for separator
The new option *2* will give limited separator "-" or "_"

**Deprecation**

**- friendly_separator :** The *friendly_separator* option will change name for *separator* in futur version.



**Warning** : 
This version didn't work with Python 2.x

Python 3.6 or higher is required


# 1.3
**New** 
**- Endpoint :** New endpoint */v2/elements*
Create ordered JSON answer splited by separator, number and words

**- Header :** New header *X-VSPG-sha256* for endpoint /*v1/password*
The X-VSPG-sha256 header contains the password hash in sha256

**- Dictionary :** Available dictionaries
 
- **en** : more than 370000 English words
- **fr** : more than 330000 French words 
- **fr_simple** : more than 22000 French words 
- **it** : more than 88300 Italian words
- **de** : more than 166100 German words
- **es** : more than 174800 Spanish words

**Deprecation**

**- friendly_separator :** The *friendly_separator* option will change name for *separator* in the next version 1.4.

# 1.4
**New** 
**- Endpoint :** New endpoint */version*
Return the version of the server in text mode.
**- Default page :** /
Retrun the basic documentation if you call the root directory instead of an error.

**- friendly_separator :** The *friendly_separator* option is now named *separator* .

**Fixe** 
 **- number_of_words :** Can now be Zero. No word will be returned.
