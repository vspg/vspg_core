openapi: 3.0.0
info:
  title: Swagger VSPG - OpenAPI 3.0
  description: |-
    Very Simple Password Generator. 
    VSPG is an API providing relatively strong password using common words.

    Build in Python . This service is designed to be run in your infrastructure and not from the Internet.


    Some useful links:
    - [The VSPG repository](https://framagit.org/vspg)
    - [The source API definition for VSPG](https://framagit.org/vspg/vspg_core/openapi.yaml)

  
  contact:
    email: vspg@otbm.fr
  license:
    name: GNU General Public License v3.0
    url: https://www.gnu.org/licenses/gpl-3.0.html
  version: 1.0.0
externalDocs:
  description: Find out more about VSPG
  url: https://vspg.otbm.fr/
servers:
  - url: https://api.vspg.otbm.fr/
tags:
  - name: elements
    description: Invidual elements of the password
  - name: password
    description: Get a full password
paths:
  /v1/elements:
    post:
      tags:
        - elements
      summary: Get Password elements
      description: Get the differents elements of the password
      operationId: postelements
      requestBody:
        description: Get Password elements
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/elements'
        required: true
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponseElements'
        '500':
          description: Invalid input
  /v1/password:
    post:
      tags:
        - password
      summary: Get full Password
      description: Get a already generated password
      operationId: dpostelementsv2
      requestBody:
        description: Get full Password
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/elements'
        required: true
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponsePassword'
        '500':
          description: Invalid input
  /v2/elements:
    post:
      tags:
        - elements
      summary: Get Password elements
      description: Get the differents elements of the password splited by types
      operationId: zpostelements
      requestBody:
        description: Get Password elements
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/elements'
        required: true
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponseElementsVdeux'
        '500':
          description: Invalid input
components:
  schemas:
    elements:
      required:
        - number_of_words
        - number_of_numbers
        - language
        - separator
      type: object
      properties:
        number_of_words:
          type: integer
          format: number
          minimum: 1
          maximum: 20
          example: 5
        number_of_numbers:
          type: integer
          format: number
          minimum: 1
          maximum: 20
          example: 4
        separator:
          type: string
          description: This is the type of separator you want to use. Check https://vspg.otbm.fr/docs/usage/
          enum:
           - 0
           - 1
           - 2  
        language:
          type: string
          description: This is the dictionary from where the words will come from. 
          enum:
            - en
            - fr
            - fr_simple
            - es
            - de
            - it
    ApiResponseElements:
      type: object
      properties:
        status:
          type: integer
          enum:
            - ok
            - ko
        result:
          type: array
          items:
            type: string
    ApiResponseElementsVdeux:
      type: object
      properties:
        status:
          type: integer
          enum:
            - ok
            - ko
        result:
          type: array
          items:
            type: object
            properties:
              separator:
                type: string
                format: string
              words:
               type: array
               items:
                type: string
              numbers:
                type: number
            
    ApiResponsePassword:
      type: object
      properties:
        status:
          type: integer
          enum:
            - ok
            - ko
        result:
          type: string
