<!doctype html>
<!-- index.tpl -->
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="VSPG is an API designed to generate simple and strong password"/>
        <meta name="robots" content="index,follow"/>
        <title>VSPG - Very Simple Password Generator</title>
</head>
<body>

<h1>VSPG - Very Simple Password Generator</h1>






<h3>Endpoint v1/elements</h3>

<b>Call:</b>
<pre>curl --request POST \
  --url http://{{!url}}/v1/elements \
  --header 'content-type: application/json' \
  --data '{
"number_of_words":"5",
"number_of_numbers":"2",
"separator":"0",
"language":"fr_simple"
}'</pre>

<b>result:</b>
<pre>{
  "status": "ok",
  "result": [
    ",",
    "07",
    "dissoute",
    "SNECMA"
  ]
}</pre>


<h3>Endpoint v1/password</h3>

<b>Call:</b>
<pre>curl --request POST \
  --url http://{{!url}}/v1/password \
  --header 'content-type: application/json' \
  --data '{
"number_of_words":"2",
"number_of_numbers":"4",
"separator":"1",
"language":"fr_simple"
}'</pre>

<b>result:</b>
<pre>{
  "status": "ok",
  "result": "MartialLudwigSubie[3048"
}</pre>


<h3>Endpoint v2/elements</h3>

<b>Call:</b>
<pre>curl --request POST \
  --url http://{{!url}}/v2/elements \
  --header 'content-type: application/json' \
  --data '{
"number_of_words":"5",
"number_of_numbers":"2",
"separator":"0",
"language":"fr_simple"
}'</pre>

<b>result:</b>
<pre>{
  "status": "ok",
  "result": [
    {
      "separator": ":",
      "number": "85",
      "words": [
        "fusion",
        "Bebes",
        "conjoints",
        "petite",
        "analytiquement"
      ]
    }
  ]
}</pre>


<h3>Endpoint v1/access_key</h3>

<b>Call:</b>
<pre>curl --request GET \
  --url http://{{!url}}/v1/access_key
</pre>

<b>result:</b>
<pre>{
    "status": "ok",
    "result": [
        {
            "access_key": "PFP5NIM1IJI69JZYCDDD",
            "secret_key": "LqIDaoFWuF0IHGuHpJXjXOCz42FScim3pfX7XzRf"
        }
    ]
}</pre>


</body>
</html>
